package com.bhushan.exception;

public class ExceptionHandlingCase3 {

	public static void main(String[] args) {
		String name = "Nitesh";

		try {
			int roll = Integer.parseInt(name);
			System.out.println(roll);
		} catch (NumberFormatException e) {
			System.out.println("Change name of Nitesh to some Integer");
		}
		

	}

}
