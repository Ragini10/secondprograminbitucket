package com.bhushan.constructor;

public class InstanceAndLocalVariable {

	public void getEmployeeData(String name, String school_name) {

		System.out.println(name + " " + school_name);

	}

	public static void main(String[] args) {
		String name = "Ragini";
		String school_name = "DAV School";

		System.out.println(name + " " + school_name);

		InstanceAndLocalVariable instanceAndLocalVariable = new InstanceAndLocalVariable();
		instanceAndLocalVariable.getEmployeeData("nitesh", "bhushan");

	}

}
